var Express = require('express');
var fs = require('fs');
const os = require('os');
const path = require('path');
const moment = require('moment');
var BodyParser = require('body-parser');
require('./logger');
var app = Express();
app.use(BodyParser.json());

app.get("/", function (req, res) {
    res.send("WELCOME!!!");
});

app.post("/uploadData", function (req, res) {
    var date = moment().format("DDMMYYYY");
    var file_name = "data_" + date + "_" + Math.floor(Math.random() * 1000) + 1;
    const filename = path.join(__dirname, `${file_name}.csv`);

    const output = []; // holds all rows of data
    output.push(["name","gender","age"]);
    const data = req.body;
    
    out = "";
    var letters = /^[A-Za-z]+$/;
    for (d of data) {
        if (d.name == "" || d.name == null || d.gender == "" || d.gender == null || d.age == "" || d.age == null) {
            out += "Field should not be empty";
        }
        if (isNaN(d.age)) {
            out += "<br>Invalid age";
        }
        if (!d.name.match(letters)) {
            out += "<br>Invalid name";
        }
        if (!d.gender.match(letters)) {
            out += "<br>" + "Invalid Gender";
        }
    }
    
    if (out == "") {
        data.forEach((d) => {
            const row = []; // a new array for each row of data
            row.push(d.name);
            row.push(d.gender);
            row.push(d.age);

            output.push(row.join()); // by default, join() uses a ','
        });
        fs.writeFileSync(filename, output.join(os.EOL));
        res.send("File uploaded successfully");
        //console.log("File uploaded successfully");

        logger.info({message:` File uploaded Successfully`,level:"info"});
    }
    else {
        res.send(out);
        //console.log(out);
        logger.error({message:`Invalid data found`,level:"error"});
    }

});

app.get("/sort", function (req, res) {
    fs.readFile("data_11022020_3151.csv", function (err, datas) {
        if (err) {
            console.log(err);
            logger.error({message:"File not found!!!",level:"error"});
            throw err;
        }
        data = datas.toString();
        arr= data.split(/\r?\n/);
        res.send(arr);
        console.log(arr);
        logger.info({message:"Data fetched Successfully",level:"info"});
    });
});

app.listen(3001, () => {
    console.log("Server is listening to port 3001....");
});