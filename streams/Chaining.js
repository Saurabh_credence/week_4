// const { pipeline } = require('stream');
// const fs = require('fs');
// const zlib = require('zlib');

// // Use the pipeline API to easily pipe a series of streams
// // together and get notified when the pipeline is fully done.
// // A pipeline to gzip a potentially huge video file efficiently:

// pipeline(
//   fs.createReadStream('data.txt'),
//   zlib.createGzip(),
//   fs.createWriteStream('data.txt.gz'),
//   (err) => {
//     if (err) {
//       console.error('Pipeline failed', err);
//     } else {
//       console.log('Pipeline succeeded');
//     }
//   }
// );


//unzip
var fs = require('fs');
var zlib = require('zlib');

fs.createReadStream('data.txt.gz')
  .pipe(zlib.createGunzip())
  .pipe(fs.createWriteStream('output.txt'));